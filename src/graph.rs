use rand::Rng;

#[allow(dead_code)]
pub fn get_all_e_for_v(graph: Vec<(i32, i32)>, v: i32) -> Vec<(i32, i32)>{
    let mut res = Vec::new();
    for edge in graph{
        let (a, b) = edge;
        if a == v || b == v{
            res.push(edge);
        }
    }
    res
}

pub fn remove_all_e_for_v(graph: &mut Vec<(i32, i32)>, v: i32){
    let mut rm_list: Vec<usize>= Vec::new();
    for i in 0..graph.len(){
        let edge = graph[i];
        let (a, b) = edge;
        if a == v || b == v{
            rm_list.push(i);
        }
    }
    rm_list.reverse();
    for i in rm_list{
        graph.remove(i);
    }
}

//generate a random graph with n vert
pub fn generate_graph(n: i32) -> Vec<(i32, i32)>{
    let mut graph = Vec::new();
    for i in 0..n{
        for j in 0..n{
            if i != j{
                if rand::thread_rng().gen(){
                    let edge = (i, j);
                    graph.push(edge);
                }
            }
        }
    }
    graph
}
