#![allow(dead_code)]
use rand::Rng;
use super::graph;

// vertexCover(E, S = ∅)
//     if E = ∅ return S
//     choose an arbitrary edge {u,v} ∈ E
//     S1 = vertexCover(E – {u,v}, S + u)
//     S2 = vertexCover(E – {u,v}, S + v)
//     return min_by_size(S1, S2)
pub fn vertexcover(graph: Vec<(i32, i32)>, s: Vec<i32>) -> Vec<i32>{
    let mut graph = graph;

    // if E = ∅ return S
    if graph.len() == 0{
        return s;
    }

    // choose an arbitrary edge {u,v} ∈ E
    let (u, v) = graph.remove(rand::thread_rng().gen_range(0..graph.len()));

    // S1 = vertexCover(E – {u,v}, S + u)
    // S2 = vertexCover(E – {u,v}, S + v)
    let mut se1 = graph.clone();
    let mut se2 = graph.clone();
    graph::remove_all_e_for_v(&mut se1, u);
    graph::remove_all_e_for_v(&mut se2, v);
    let mut ss1 = s.clone();
    let mut ss2 = s.clone();
    ss1.push(u);
    ss2.push(v);
    let s1 = vertexcover(se1, ss1);
    let s2 = vertexcover(se2, ss2);

    //retugn the smallest one
    if s1.len() < s2.len(){
        s1
    }else{
        s2
    }
}

//cormen approximations vertex cover
// CORMEN(Graph G = (V, E)) 1
// S ← ∅
// while E ≠ ∅ do
//     choose arbitrary edge {u, v} ∈ E
//     S ← S ∪ {u, v}
//     remove {u, v} from E and every other edge incident either on u or v
// return S
pub fn vertexcover_cormen(graph: Vec<(i32, i32)>) -> Vec<i32>{
    let mut graph = graph;

    // S ← ∅
    let mut s = Vec::new();

    // while E ≠ ∅ do
    while !graph.is_empty(){

        //choose an arbitrary edge {u,v} ∈ E
        let rdm_index = rand::thread_rng().gen_range(0..graph.len());
        let (u, v) = graph.remove(rdm_index);

        // S ← S ∪ {u, v}
        s.push(u);
        s.push(v);

        // remove {u, v} from E and every other edge incident either on u or v
        graph::remove_all_e_for_v(&mut graph, u);
        graph::remove_all_e_for_v(&mut graph, v);
    }
    s   
}
// vertexCover(Graph G = (V, E), k, S = ∅)
//     if E = ∅ return S
//     if k = 0 return no-instance
//     choose an arbitrary edge {u, v} ∈ E
//     // N[u] -> Alle Kanten, die mit u verbunden sind
//     // N[v] -> Alle Kanten, die mit v verbunden sind
//     S1 = vertexCover((V – u, E – N[u]), k – 1, S + u)
//     S2 = vertexCover((V – v, E – N[v]), k – 1, S + v)
//     if S1 and S2 are no-instances return no-instance
//     if S1 is a no-instance return S2
//     if S2 is a no-instance return S1
//     return min_by_size(S1, S2)
pub fn vertexcover_bounded(graph: Vec<(i32, i32)>, k: i32, s: Vec<i32>) -> Option<Vec<i32>>{
    let mut graph = graph;

    // if E = ∅ return S
    if graph.len() == 0{
        return Some(s);
    }

    // if k = 0 return no-instance
    if k == 0{
        return None;
    }

    // choose an arbitrary edge {u, v} ∈ E
    let (u, v) = graph.remove(rand::thread_rng().gen_range(0..graph.len()));

    // N[u] -> Alle Kanten, die mit u verbunden sind
    // N[v] -> Alle Kanten, die mit v verbunden sind
    // S1 = vertexCover((V – u, E – N[u]), k – 1, S + u)
    // S2 = vertexCover((V – v, E – N[v]), k – 1, S + v)
    let mut se1 = graph.clone();
    let mut se2 = graph.clone();
    graph::remove_all_e_for_v(&mut se1, u);
    graph::remove_all_e_for_v(&mut se2, v);
    let mut ss1 = s.clone();
    let mut ss2 = s.clone();
    ss1.push(u);
    ss2.push(v);
    let s1 = vertexcover_bounded(se1, k-1, ss1);
    let s2 = vertexcover_bounded(se2, k-1, ss2);
    // if S1 and S2 are no-instances return no-instance
    if s1.is_none() && s2.is_none(){
        return None;
    }
    // if S1 is a no-instance return S2
    if s1.is_none(){
        return s2;
    }
    
    // if S2 is a no-instance return S1
    if s2.is_none(){
        return s1;
    }
    
    // return min_by_size(S1, S2)
    if s1.as_ref().unwrap().len() < s2.as_ref().unwrap().len(){
        s1
    }else{
        s2
    }

}
