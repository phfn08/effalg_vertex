#![allow(dead_code)]
use std::time::{Instant, Duration};
mod vertex;
mod graph;

fn duration_to_int(duration: Duration) -> i64{
    duration.as_secs() as i64 * 1000000000 + duration.subsec_nanos() as i64
}
fn duration_div(a: Duration, b: Duration) -> f64{
    if b == Duration::new(0, 0){
        return 0.0;
    }
    let a_int = duration_to_int(a);
    let b_int = duration_to_int(b);
    (a_int as f64) / (b_int as f64)
}
fn main_aprx(){
    let m: usize= 100;
    for n in [5, 10, 15].iter(){
        println!("random graph with {:?} vertices", n);
        let mut duration_cormen = Duration::new(0, 0);
        let mut durations_cormen: Vec<Duration> = Vec::new();
        let mut length_cormen = 0;
        let mut lengths_cormen: Vec<usize> = Vec::new();
        let mut duration_mvc = Duration::new(0, 0);
        let mut durations_mvc: Vec<Duration> = Vec::new();
        let mut length_mvc = 0;
        let mut lengths_mvc: Vec<usize> = Vec::new();
        for _ in 0..m{

            let graph = graph::generate_graph(*n);
            // println!("{:?}", graph);
            let start = Instant::now();
            let s_cormen = vertex::vertexcover_cormen(graph.clone());
            duration_cormen += start.elapsed();
            length_cormen += s_cormen.len();
            durations_cormen.push(start.elapsed());
            lengths_cormen.push(s_cormen.len());


            let start = Instant::now();
            let s = vertex::vertexcover(graph.clone(), vec![]);
            duration_mvc += start.elapsed();
            length_mvc += s.len();
            durations_mvc.push(start.elapsed());
            lengths_mvc.push(s.len());

        }
        let avg_duration_cormen = duration_cormen / m as u32;
        let avg_duration_mvc = duration_mvc / m as u32;
        let avg_length_cormen = length_cormen as f64 / m as f64;
        let avg_length_mvc = length_mvc as f64 / m as f64;

        println!("cormen mean time: {:?}, mean length: {:?}", avg_duration_cormen, avg_length_cormen);
        println!("mvc mean time: {:?}, mean length: {:?}", avg_duration_mvc, avg_length_mvc);
        println!("cormen is {:.1} times faster, but {:.1} times bigger", duration_div(duration_mvc, duration_cormen), length_cormen as f64 / length_mvc as f64);
        println!();
    }
}

fn main_bounded(){
    let k = 3;
    let graph = graph::generate_graph(5);
    println!("{:?}", graph);
    let result = vertex::vertexcover_bounded(graph, k, vec![]);
    println!("{:?}", result);



}
fn main() {
    main_aprx();
    // main_bounded();
}
